//
//  ProgressView.swift
//  Digital Workers
//
//  Created by Aksonsat Uanthoeng on 8/22/2560 BE.
//  Copyright © 2560 Depadigitalworkforce. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView

// MARK: - NVActivityIndicatorViewable
extension UIViewController: NVActivityIndicatorViewable {
    
    func startLoader(progressView:UIProgressView) {
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "กำลังโหลดข้อมูล", type: NVActivityIndicatorType.ballTrianglePath, color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), textColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
        
        progressView.progress = 0.0
        progressView.isHidden = false
    }
    
    func stoptLoader(progressView:UIProgressView) {
        // ใส่ดีเลย์ ไป 1 วิ เพื่ิอจะได้เห็นตัวโหลดขึ้น ถ้าไม่เอาก็ลบได้ เหลือแค่ self.stopAnimating()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
            self.stopAnimating()
            progressView.isHidden = true
        }
        //self.stopAnimating()
        //self.progressView.isHidden = true
    }
}

