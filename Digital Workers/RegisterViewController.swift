//
//  RegisterViewController.swift
//  Digital Workers
//
//  Created by Aksonsat Uanthoeng on 8/30/2560 BE.
//  Copyright © 2560 Depadigitalworkforce. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ActionSheetPicker_3_0

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var textfieldFirstName: UITextField!
    @IBOutlet weak var textfieldLastName: UITextField!
    @IBOutlet weak var textfieldEmail: UITextField!
    @IBOutlet weak var textfieldPassword: UITextField!
    @IBOutlet weak var textfieldConfirmPassword: UITextField!
    @IBOutlet weak var textfieldType: UITextField!
    @IBOutlet weak var buttonType: UIButton!
    @IBOutlet weak var buttonRegister: UIButton!
    
    var strFirstName:String = ""
    var strLastName:String = ""
    var strEmail:String = ""
    var strPassword:String = ""
    var strConfirmPassword:String = ""
    var strType:String = ""
    
    let progressView: UIProgressView = UIProgressView(frame: CGRect(x: 0, y: 0, width: 100, height: 60))
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.hideKeyboardWhenTappedAround()
        self.textfieldType.text = "กรุณาเลือกประเภทผู้สมัคร"
        self.textfieldPassword.isSecureTextEntry = true
        self.textfieldConfirmPassword.isSecureTextEntry = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    @IBAction func selectUserType(sender:UIButton) {
        let typeStr = ["ผู้ทำงาน","ผู้ประกอบการ"]
        let typeValue = ["worker","investor"]
        if !typeStr.isEmpty {
            let dataPicker = ActionSheetStringPicker(title: "เลือกประเภท", rows: typeStr , initialSelection: 0, doneBlock: {
                picker, indexes, values in
                    self.textfieldType.text = "\(values ?? "No data")"
                    self.strType = typeValue[indexes]
                return
            }, cancel: nil, origin: sender)
            dataPicker?.show()
        } else {
            print("Error.")
        }
    }
    
    @IBAction func submitRegister(sender:UIButton) {
        self.strFirstName = textfieldFirstName.text!
        self.strLastName = textfieldLastName.text!
        self.strEmail = textfieldEmail.text!
        self.strPassword = textfieldPassword.text!
        self.strConfirmPassword = textfieldConfirmPassword.text!
        
        //print("\(strFirstName) \(strLastName) \(strEmail) \(strPassword) \(strConfirmPassword) \(strType)")
        if self.strFirstName == "" || self.strLastName == "" || self.strEmail == "" || self.strPassword == "" || self.strConfirmPassword == "" || self.strType == "" {
            //Alertview
            let alert = UIAlertController(title: "Digital Workers", message: "กรุณาเพิ่มข้อมูลให้ครบถ้วน", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "ตกลง", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            // Call JSON Register
            if self.strPassword == self.strConfirmPassword {
                self.jsonRegister(firstName: self.strFirstName, lastName: self.strLastName, email: self.strEmail, passwd: self.strPassword, confirmPasswd: self.strConfirmPassword, type: self.strType)
            } else {
                let alert = UIAlertController(title: "Digital Workers", message: "รหัสผ่านไม่ตรงกัน", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "ตกลง", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    /** Parsing Json **/
    func jsonRegister(firstName:String, lastName:String, email:String, passwd:String, confirmPasswd:String, type:String) {
        self.startLoader(progressView: self.progressView)
        //self.showLoading(isShow: true)
        let headers: HTTPHeaders = [
            "Content-Type":"application/x-www-form-urlencoded"]
        let urlPath: String =  "https://www.depadigitalworkforce.com/indexapp_investor.php"
        let params: Parameters = [
            "first_name": "\(firstName)",
            "last_name": "\(lastName)",
            "email": "\(email)",
            "password": "\(passwd)",
            "password_confirm": "\(confirmPasswd)",
            "type": "\(type)"
        ]
        
        Alamofire.request(urlPath, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                //print("Json response = \(swiftyJsonVar["status"])")
                
                let statusRespone = swiftyJsonVar["status"].bool
                let msgRespone = swiftyJsonVar["message"].string
                
                self.stoptLoader(progressView: self.progressView)

                if statusRespone! {
                    let alert = UIAlertController(title: "Digital Workers", message: "\(msgRespone ?? "No value")", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "ตกลง", style: UIAlertActionStyle.default, handler: { action in
                        
                        self.textfieldFirstName.text = ""
                        self.textfieldLastName.text = ""
                        self.textfieldEmail.text = ""
                        self.textfieldPassword.text = ""
                        self.textfieldConfirmPassword.text = ""
                        self.textfieldType.text = "กรุณาเลือกประเภทผู้สมัคร"
                        
                        self.tabBarController?.selectedIndex = 1
                    }))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let alert = UIAlertController(title: "Digital Workers", message: "\(msgRespone ?? "No value")", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "ตกลง", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
}
