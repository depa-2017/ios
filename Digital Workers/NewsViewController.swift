//
//  NewsViewController.swift
//  Digital Workers
//
//  Created by Aksonsat Uanthoeng on 8/30/2560 BE.
//  Copyright © 2560 Depadigitalworkforce. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher
//import Kingfisher

class NewsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView:UITableView!
    let progressView: UIProgressView = UIProgressView(frame: CGRect(x: 0, y: 0, width: 100, height: 60))
    
    var newsJson:[JSON] = []
    
    let userDefaults = UserDefaults.standard
    //let global:Global = Global()

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.jsonParsingNewsl()
    }
    
    // MARK: - TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsJson.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:NewsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "NewsTableViewCell", for: indexPath) as! NewsTableViewCell
        
        //print("News Title : \(newsJson[indexPath.row]["newhand_name"])")
        
        let url = URL(string: "https://www.depadigitalworkforce.com/img/news/\(newsJson[indexPath.row]["newhand_pic"])")
        //print("Image url : \(url)")
        cell.imageNews.kf.setImage(with: url)
        cell.lblTitle.text = "\(newsJson[indexPath.row]["newhand_name"])"
        cell.lblShotDes.text = "\(newsJson[indexPath.row]["newhand_date"])"
        // https://www.depadigitalworkforce.com/img/news/{newhand_pic}
        //cell.imageForum.kf.setImage(with: url)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "NewsDetailNavViewController") as! NewsDetailNavViewController
        //let navController = UINavigationController(rootViewController: VC1)
        //let storyboardInputDetail = UIStoryboard(name: "Vocab", bundle: nil)
        //let detail = storyboardInputDetail.instantiateViewController(withIdentifier: "DetailNavigationViewController") as! DetailNavigationViewController
        
        self.userDefaults.set("\(newsJson[indexPath.row]["news_id"])", forKey: Global.newsId)
        self.userDefaults.set("\(newsJson[indexPath.row]["newhand_name"])", forKey: Global.newsTitle)
        
        self.present(VC1, animated: true, completion: nil)
        
        /*
        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "MyViewController") as! MyViewController
        let navController = UINavigationController(rootViewController: VC1)
        self.present(navController, animated:true, completion: nil)
        */
    }
    
    /** Parsing Json **/
    func jsonParsingNewsl() {
        //self.showLoading(isShow: true)
        self.startLoader(progressView: self.progressView)
        let urlPath: String =  "https://www.depadigitalworkforce.com/indexapp_new.php"
        Alamofire.request(urlPath, method: .get, encoding: JSONEncoding.default).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                
                let swiftyJsonVar = JSON(responseData.result.value!)
                self.newsJson = swiftyJsonVar["results"].arrayValue
                //self.newsJson = swiftyJsonVar.array!
                
                print("Swift JSON = \(self.newsJson)")
                
                self.stoptLoader(progressView: self.progressView)
                
                if !self.newsJson.isEmpty {
                    self.tableView.isHidden = false
                    
                    //self.dictNews = dataNewss.arrayObject as! [[String : Any]]
                    //self.dictPromotion = dataPromotions.arrayObject as! [[String : Any]]
                    //self.dictBrand = dataBrands.arrayObject as! [[String : Any]]
                    
                    //print("Data in jsonParsing : \(self.dictPost)")
                    self.tableView.reloadData()
                } else {
                    self.startLoader(progressView: self.progressView)
                    self.alert(strMsg: "ไม่พบข้อมูลข่าวสาร")
                }
            } else {
                self.stoptLoader(progressView: self.progressView)
                //self.showLoading(isShow: false)
                self.alert(strMsg: "เกิดข้อผิดพลาดในการเชื่อมต่อกับเซิร์ฟเวอร์")
            }
        }
    }
    
    func alert(strMsg:String) {
        let alert_submit = UIAlertController(title: nil, message: strMsg as String, preferredStyle: .alert)
        alert_submit.addAction(UIAlertAction(title: "ตกลง", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert_submit, animated: true, completion: nil)
    }
}
