//
//  FacebookViewController.swift
//  Digital Workers
//
//  Created by Aksonsat Uanthoeng on 8/23/2560 BE.
//  Copyright © 2560 Depadigitalworkforce. All rights reserved.
//

import UIKit

class FacebookViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView:UIWebView!
    
    //let randomNum:UInt32 = arc4random_uniform(5)
    
    //Add this progress view via Interface Builder (IBOutlet) or programatically
    let progressView: UIProgressView = UIProgressView(frame: CGRect(x: 0, y: 0, width: 100, height: 60))

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //webView.loadRequest(URLRequest(url: URL(string: "https://m.facebook.com/")!))
        //let fbURL = NSURL(string: "https://www.facebook.com/")
        //let request = NSURLRequest(url: fbURL! as URL)
        //webView.loadRequest(request as URLRequest)depachiangmai/
        

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        /**
         NSURL *url = [NSURL URLWithString:@"fb://profile/<id>"];
         [[UIApplication sharedApplication] openURL:url];
        **/
        
        //let sUrl = "fb://depachiangmai/224212310923331" 100001240422886
        //UIApplication.shared.openURL(NSURL(string: sUrl)! as URL)
        if let url = URL(string: "fb://profile/224212310923331") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],completionHandler: { (success) in
                    print("Open fb://profile/224212310923331: \(success)")
                })
            } else {
                let success = UIApplication.shared.openURL(url)
                print("Open fb://profile/224212310923331: \(success)")
            }
        }
        
        self.tabBarController?.selectedIndex = 0
        /**
        if let url = URL(string: "https://m.facebook.com/") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:])
            } else {
                // Fallback on earlier versions
            }
        }
        **/
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        //self.startLoader(progressView: self.progressView)
        print("Start")
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.stoptLoader(progressView: self.progressView)
        print("Stop")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
