//
//  NewsDetailViewController.swift
//  Digital Workers
//
//  Created by Aksonsat Uanthoeng on 10/5/2560 BE.
//  Copyright © 2560 Depadigitalworkforce. All rights reserved.
//

import UIKit

class NewsDetailViewController: UIViewController {
    
    @IBOutlet weak var scrollView:UIScrollView!
    
    var detailView: NewsDetailView = NewsDetailView()
    
    let userDefaults = UserDefaults.standard
    let global:Global = Global()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.scrollView.contentSize = CGSize(width: self.view.frame.width, height: self.detailView.view.frame.size.height)
        self.scrollView.addSubview(self.detailView.view)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        //let title = "\(userDefaults.string(forKey: global.vocabTitleTH)!)"
 
        //self.navigationController?.navigationBar.titleTextAttributes = attributes
        //self.navigationController?.navigationBar.topItem?.title = title
        
        
        
    }
    
    @IBAction func actionBack(sender:UIBarButtonItem) {
        //print("Back")
        //self.animationLeftToRight()
        self.dismiss(animated: false, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
