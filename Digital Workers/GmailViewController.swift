//
//  GmailViewController.swift
//  Digital Workers
//
//  Created by Aksonsat Uanthoeng on 8/23/2560 BE.
//  Copyright © 2560 Depadigitalworkforce. All rights reserved.
//

import UIKit
import MessageUI

class GmailViewController: UIViewController, MFMailComposeViewControllerDelegate{
    
    @IBOutlet weak var webView:UIWebView!
    
    //let check = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
        
        // https://mail.google.com/mail/u/0/?tab=wm#inbox
        //webView.loadRequest(URLRequest(url: URL(string: "https://mail.google.com/mail/u/0/?tab=wm#inbox")!))
        //webView.loadRequest(URLRequest(url: URL(string: "https://mail.google.com/mail/u/0/?view=cm&fs=1&to=chiangmai@depa.or.th&su=Send+From+Application&body=เพิ่มข้อความของคุณที่นี่&tf=1")!))
        
        /**
        if let url = URL(string: "https://mail.google.com/mail/u/0/?view=cm&fs=1&to=chiangmai@depa.or.th&su=Send+From+Application&body=เพิ่มข้อความของคุณที่นี่&tf=1") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:])
            } else {
                // Fallback on earlier versions
            }
        }
        **/
        /**
        // Do any additional setup after loading the view.
        let googleUrlString = "googlegmail:///co?subject=Hello&body=Hi"
        if let googleUrl = NSURL(string: googleUrlString) {
            // show alert to choose app
            if UIApplication.shared.canOpenURL(googleUrl as URL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(googleUrl as URL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(googleUrl as URL)
                }
            }
        }
        **/
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.selectedIndex = 0
        
        /**
        let sUrl = "googlechrome://https://mail.google.com/mail/u/0/?view=cm&fs=1&to=chiangmai@depa.or.th&su=Send+From+Application&body=เพิ่มข้อความของคุณที่นี่&tf=1"
        UIApplication.shared.openURL(NSURL(string: sUrl)! as URL)
        **/
        /**
        if let url = URL(string: "https://mail.google.com/mail/u/0/?view=cm&fs=1&to=chiangmai@depa.or.th&su=Send+From+Application&body=เพิ่มข้อความของคุณที่นี่&tf=1") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:])
            } else {
                // Fallback on earlier versions
            }
        }
        **/
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["chiangmai@depa.or.th"])
        mailComposerVC.setSubject("เพิ่มหัวข้อคุณที่นี่")
        mailComposerVC.setMessageBody("เพิ่มเนื้อหาของคุณที่นี่", isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
