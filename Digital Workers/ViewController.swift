//
//  ViewController.swift
//  Digital Workers
//
//  Created by Aksonsat Uanthoeng on 8/10/2560 BE.
//  Copyright © 2560 Depadigitalworkforce. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView:UIWebView!
    
    //let randomNum:UInt32 = arc4random_uniform(5)
    
    //Add this progress view via Interface Builder (IBOutlet) or programatically
    let myProgressView: UIProgressView = UIProgressView(frame: CGRect(x: 0, y: 0, width: 100, height: 60))
    
    var theBool: Bool = false
    var myTimer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        webView.loadRequest(URLRequest(url: URL(string: "https://www.depadigitalworkforce.com/")!))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        //print("Start.")
        self.startLoader(progressView: myProgressView)
        
        //let num:Float = Float(randomNum)
        
        let when = DispatchTime.now() + 5 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            // Your code with delay
            self.stoptLoader(progressView: self.myProgressView)
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("WebView Finish.")
    }    
}

