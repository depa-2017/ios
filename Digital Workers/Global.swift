//
//  Global.swift
//  IPST Science Vocabulary
//
//  Created by Aksonsat Uanthoeng on 7/7/2560 BE.
//  Copyright © 2560 Diversition. All rights reserved.
//

import Foundation

class Global {
    
    static public let newsId: String = "NEWS_ID"
    static public let newsTitle: String = "NEWS_TITLE"
    
}
