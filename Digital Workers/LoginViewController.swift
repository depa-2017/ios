//
//  LoginViewController.swift
//  Digital Workers
//
//  Created by Aksonsat Uanthoeng on 8/30/2560 BE.
//  Copyright © 2560 Depadigitalworkforce. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView:UIWebView!
    
    let progressView: UIProgressView = UIProgressView(frame: CGRect(x: 0, y: 0, width: 100, height: 60))

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        webView.loadRequest(URLRequest(url: URL(string: "https://www.depadigitalworkforce.com/index.php?folder=register&file=register_form")!))
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        //print("Start.")
        //self.startLoader(progressView: progressView)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        //self.stoptLoader(progressView: self.progressView)
    }

}
