//
//  NewsDetailView.swift
//  Digital Workers
//
//  Created by Aksonsat Uanthoeng on 10/5/2560 BE.
//  Copyright © 2560 Depadigitalworkforce. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher

class NewsDetailView: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    let progressView: UIProgressView = UIProgressView(frame: CGRect(x: 0, y: 0, width: 100, height: 60))

    let userDefaults = UserDefaults.standard
    
    var newsJson:[JSON] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.isHidden = true

        // Do any additional setup after loading the view.
        let newsId = userDefaults.string(forKey: Global.newsId)
        jsonParsingNewslDetail(newsId: newsId!)
        //self.navigationItem.title = "JDK"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /** Parsing Json **/
    func jsonParsingNewslDetail(newsId:String) {
        //self.showLoading(isShow: true)
        self.startLoader(progressView: self.progressView)
        let urlPath: String =  "https://www.depadigitalworkforce.com/indexapp_newdetail.php?new_id=\(newsId)"
        Alamofire.request(urlPath, method: .get, encoding: JSONEncoding.default).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                
                let swiftyJsonVar = JSON(responseData.result.value!)
                self.newsJson = swiftyJsonVar["results"].arrayValue
                //self.newsJson = swiftyJsonVar.array!
                
                //print("Swift JSON Detail = \(self.newsJson)")
                
                self.stoptLoader(progressView: self.progressView)
                
                if !self.newsJson.isEmpty {
                    
                    let url = URL(string: "https://www.depadigitalworkforce.com/img/news/\(self.newsJson[0]["newhand_pic"])")
                    //print("Image url : \(url)")
                    self.imageView.kf.setImage(with: url)
                    self.lblTitle.text = "\(self.newsJson[0]["newhand_name"])"
                    
                    let desString = self.newsJson[0]["news_detail"].string
                    self.lblDescription.text = desString?.html2String
                    
                    self.view.isHidden = false
                } else {
                    self.startLoader(progressView: self.progressView)
                    self.alert(strMsg: "ไม่พบข้อมูลข่าวสาร")
                }
            } else {
                self.stoptLoader(progressView: self.progressView)
                //self.showLoading(isShow: false)
                self.alert(strMsg: "เกิดข้อผิดพลาดในการเชื่อมต่อกับเซิร์ฟเวอร์")
            }
        }
    }
    
    func alert(strMsg:String) {
        let alert_submit = UIAlertController(title: nil, message: strMsg as String, preferredStyle: .alert)
        alert_submit.addAction(UIAlertAction(title: "ตกลง", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert_submit, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
