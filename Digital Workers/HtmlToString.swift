//
//  HtmlToString.swift
//  Digital Workers
//
//  Created by Aksonsat Uanthoeng on 10/5/2560 BE.
//  Copyright © 2560 Depadigitalworkforce. All rights reserved.
//

import Foundation
import UIKit

extension String {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: Data(utf8), options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
