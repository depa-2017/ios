//
//  Hidekeyboard.swift
//  Digital Workers
//
//  Created by Aksonsat Uanthoeng on 8/31/2560 BE.
//  Copyright © 2560 Depadigitalworkforce. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}

